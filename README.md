references:
  https://richardballard.co.uk/aws-lambda-function-for-dotnet-3.1-using-custom-runtime/

```
dotnet lambda deploy-function --name testApp --region us-east-2
dotnet lambda invoke-function --name testApp --region us-east-2 -p "test"
```
